# Voice Recognition

Simple "Yandex vs Google" voice recognition experiment.

Google documentation:
http://developer.android.com/intl/ru/reference/android/speech/SpeechRecognizer.html

Yandex documentation:
https://tech.yandex.ru/speechkit/mobilesdk/

![(screenshot)](/screens/screen01.png?raw=true "(screenshot)")